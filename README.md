# Task 1: Terraform infrastructure modification

> **Priority: High**

> **Story points: 2**

We have a new DevOps engineer (new.devops@company.com) joining the team. You need to update our GCP staging platform to give editor rights to the new team member.  Given the Terraform staging configuration folder, add the new user and deploy the changes.

Deliverable:

- [ ] GitLab MR with changes to include the new team member.
- [ ] List of commands to be executed to apply the new configuration as part of the MR description.

Resources:

- [ ] This public GitLab repository.

# Task 2: Deploy a k3s  cluster and install a pod

> **Priority: High**

> **Story points: 5**

To prepare the deployment of new microservices our DevOps team is asked to start a k3s instance and deploy a Prometheus pod to gather metrics. This task wants you to access a GCP remote instance (ssh to a given IP, please provide your public ssh-key).

Deliverable:

- [ ] A completed deployment script in (`infrastructure/k3s/deploy.sh`) containing a list of commands to deploy k3s and Prometheus.

Resources:

- [ ] Remote instance: IP provided during the interview.

Pod to be deployed: https://bitnami.com/stack/prometheus-operator/helm .

# Task 3: Support team member to modify a Gitlab CI pipeline

> **Priority: High**

> **Story points: 2**

A colleague needs to modify our current CI to fail the unit test step if coverage is lower than 80%. She has little experience with GitLab CI and comes to you seeking help and shows you the current CI, please advise her on how to proceed.

Deliverable:

- [ ] Support your colleague by modifying the CI pipeline and create a MR with your changes.

Resources:

- [ ] This public GitLab repository with an MR with the changes she proposes.

# Task 4: Find a virtualization tool that allows changing MAC address

> **Priority: Medium**

> **Story points: 2**

A third-party software works with a license that is issued to run in a machine with a predefined MAC address. We want to circumvent this limitation through virtualization. How can we do this?

Deliverable:

- [ ] A text file named task4.txt, with three proposals with pros and cons, uploaded to this repository.


# Task 5: Preparation for a cloud security auditory 

> **Priority: Medium**

> **Story points: 1**

There will be a security auditory in our GCP-hosted cloud infrastructure prior to a bug bounty round. We want to make sure that our infrastructure follows best practices in terms of security, however, because it has grown organically this is not guaranteed. Your task is to identify three comprehensive sources of industry security best practices and make a recommendation on which infrastructure areas are more relevant to check first.

Deliverable:

- [ ] A text file named task5.txt with your answers uploaded to this repository.

