output "customer_mongodb_instance_ips" {
  value = google_compute_instance.customer_mdb.*.network_interface.0.network_ip
}

output "bastion_external_ip" {
  value = google_compute_instance.bastion.network_interface.0.access_config.0.nat_ip
}

output "sops_kms_keys" {
  value = module.sops_kms.keys
}