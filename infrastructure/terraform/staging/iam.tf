module "owner_iam_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 6.4"

  projects = [var.project_id]
  mode     = "additive"

  bindings = {
    "roles/owner"  = formatlist("user:%s", var.iam_owner_users)
    "roles/editor" = formatlist("user:%s", var.iam_editor_users)
    "roles/viewer" = formatlist("user:%s", var.iam_viewer_users)
  }
}

module "gitlab_terraform_service_account" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 2.0"
  project_id = var.project_id

  names = ["gitlab-terraform-${var.env}"]
  project_roles = [
    "${var.project_id}=>roles/owner",
    "${var.project_id}=>roles/cloudkms.admin",
    "${var.project_id}=>roles/iam.serviceAccountAdmin",
    "${var.project_id}=>roles/resourcemanager.projectIamAdmin",
  ]
}
