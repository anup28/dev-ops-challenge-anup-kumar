project_id = "staging-296044"
region     = "us-central1"
env        = "staging"

# Network
bastion_vpc_cidr       = "10.30.1.0/24"
customer_mdb_vpc_cidr  = "10.30.50.0/24"
devops_gke_vpc_cidr     = "10.30.96.0/21"
elasticsearch_vpc_cidr = "10.30.104.0/24"

# MongoDB
customer_mdb_node_count = 1
customer_mdb_disk_size  = "128"
#customer_mdb_instance_type = "g1-small"
customer_mdb_instance_type = "n1-standard-1"

# IAM
## Project owners
iam_owner_users = [
  "owner1@company.com",
  "owner2@company.com",
  "owner3@company.com",
]

iam_editor_users = [
  "editor1@company.com",
  "editor2@company.com",
  "editor3@company.com",
]

iam_viewer_users = []

iam_sops_users = [
  "owner1@company.com",
  "owner2@company.com",
  "owner3@company.com",
  "editor1@company.com",
  "editor2@company.com",
  "editor3@company.com",
]

## SSH
iam_ssh_keys = {
  "owner1" = "ssh-rsa AAAAB3NzaC2yc2EAAAADAQABAAABgyCmJzGMrWxNMYB9Gn75dicZqoyJkj6/R3KpxwNzw1vbZtnWliJMOGSx9ID64YFJPDtJCMNNJDktxda6ZnGq1xPrg7suefFcuHfvwmAaI4ow0Oa14S9jpM+1uBXIvxmzAXqYFmFHq34LlkNQ4xzMC/1u6uzLMk/JaqurvoyipC5Bwjrt65OKu4ufa9yP8qlgpBDCq4YOer9P3GdkN0FtCbHOzjdy5LXlqajDGTj4WsPqn3UR2/nf3TFLgT6NHeWx100bjSKks/j40L9JWxgQ6fU0SxvSYHvUnsc0bob5ofxQbmivQkJOnwTmNsRI54UUEDjujqOlrpVuHZ5d09o/3OaNjsM6COooe8d0vB9DFTaOoz8KBu8B+6LsjhH9hN/NlpXv3vmicx6hkzYqjWXZMikGvOLwMxRl+rLC9JLl+M63g2xky6uOvh+Ic4H1/Yuhsc22WH/B5ZghgShptDHT1EheIBqvwlu1MMIegNf2gLcwybjUsywKv61ivLAeKY+oRe8= user1@computer"
}

bastion_user_list = [
  "owner1",
  "owner2",
  "owner3",
  "editor1",
  "editor2",
  "editor3",
]