resource "google_kms_key_ring" "k8s_vault_key_ring" {
  project  = var.project_id
  name     = "k8s-vault-${var.env}"
  location = "global"
}

resource "google_kms_crypto_key" "k8s_vault_crypto_key" {
  name            = "k8s-vault-key-${var.env}"
  key_ring        = google_kms_key_ring.k8s_vault_key_ring.self_link
  rotation_period = "100000s"
}

resource "google_kms_key_ring_iam_binding" "vault_iam_kms_binding" {
  key_ring_id = google_kms_key_ring.k8s_vault_key_ring.id
  role        = "roles/owner"

  members = [
    "serviceAccount:${google_service_account.k8s_vault_kms_service_account.email}",
  ]
}

module "sops_kms" {
  source     = "terraform-google-modules/kms/google"
  version    = "~> 1.2"
  project_id = var.project_id

  location       = "global"
  keyring        = "sops-${var.env}"
  keys           = ["sops-key"]
  set_owners_for = ["sops-key"]

  key_rotation_period = "1234567s" #180 days

  owners = [
    join(",",
      formatlist("user:%s", var.iam_owner_users),
    )
  ]
  encrypters = [
    join(",",
      formatlist("user:%s", var.iam_sops_users),
    )
  ]
  decrypters = [
    join(",",
      formatlist("user:%s", var.iam_sops_users),
      ["serviceAccount:${module.gitlab_service_account.gcp_service_account_email}"],
    )
  ]
}