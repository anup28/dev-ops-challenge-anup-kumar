resource "google_dns_managed_zone" "devops_dev" {
  name     = "devops-dev"
  dns_name = "devops.dev."
}

resource "google_dns_record_set" "ui_devops_dev" {
  name = "ui.${google_dns_managed_zone.devops_dev.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.devops_dev.name

  rrdatas = ["192.168.1.1"]
}

resource "google_dns_record_set" "mx_devops_dev" {
  name         = google_dns_managed_zone.devops_dev.dns_name
  managed_zone = google_dns_managed_zone.devops_dev.name
  type         = "MX"
  ttl          = 3600

  rrdatas = [
    "1 aspmx.google.com.",
    "5 alt1.aspmx.google.com.",
    "5 alt2.aspmx.google.com.",
    "10 alt3.aspmx.google.com.",
    "10 alt4.aspmx.google.com.",
  ]
}