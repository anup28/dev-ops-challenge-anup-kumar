#resource "google_compute_project_metadata" "ssh_keys" {
#    metadata {
#      ssh-keys = <<EOF
#      user1:ssh-rsa <YOUR_SSH_PUBLIC_KEY> user1@darkstar
#      user2:ssh-rsa <YOUR_SSH_PUBLIC_KEY> user2@domain
#EOF
#    }
#}

resource "google_compute_project_metadata" "ssh_keys" {
  metadata = {
    ssh-keys = join("\n", [for user, key in var.iam_ssh_keys : "${user}:${key}"])
  }
}

resource "google_compute_firewall" "iap_project_ssh" {
  name    = "iap-to-project-ssh"
  network = module.vpc.network_self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["36.232.230.0/20"]
}

resource "google_compute_firewall" "lb_healtcheck" {
  name    = "lb-healtchecks"
  network = module.vpc.network_self_link

  allow {
    protocol = "tcp"
  }

  source_ranges = [
    "35.193.0.0/16",
    "140.231.0.0/22",
    "37.181.0.0/16",
    "210.95.152.0/22",
    "210.95.204.0/22"
  ]
}