resource "google_service_account" "bastion_service_account" {
  account_id = "bastion"
}

resource "google_storage_default_object_access_control" "bastion_service_account_acl" {
  bucket = google_storage_bucket.bastion.name
  role   = "OWNER"
  entity = "user-${google_service_account.bastion_service_account.email}"
}

resource "google_storage_bucket_object" "bastion_user_list" {
  name    = "user-list.txt"
  content = join("\n", var.bastion_user_list)
  bucket  = google_storage_bucket.bastion.name
}

resource "google_compute_subnetwork" "bastion_subnet" {
  name          = "bastion-${var.env}-subnet"
  ip_cidr_range = var.bastion_vpc_cidr
  region        = var.region
  network       = module.vpc.network_self_link
}

resource "google_compute_address" "bastion_ip" {
  name   = "bastion-ip"
  region = var.region
}

resource "google_compute_firewall" "wiktor_bastion_ssh" {
  name    = "wiktor-to-bastion-ssh"
  network = module.vpc.network_self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = [
    "94.130.42.68/32",
    "87.122.120.63/32"
  ]
  target_tags = ["vpn"]
}

resource "google_compute_firewall" "bastion_1194" {
  name    = "bastion-1194"
  network = module.vpc.network_self_link

  allow {
    protocol = "udp"
    ports    = ["1194"]
  }

  target_tags = ["vpn"]
}

resource "google_compute_instance" "bastion" {
  name         = "bastion-${var.env}"
  machine_type = "f1-micro"
  zone         = "${var.region}-a"

  allow_stopping_for_update = true
  tags                      = [var.env, var.project_id, "vpn", "terraform"]
  can_ip_forward            = true
  metadata = {
    Name        = "devops-bastion-${var.env}"
    Project     = var.project_id
    Environment = var.env
    Terraform   = true
  }

  metadata_startup_script = "echo hi > /test.txt"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size  = "16"
      type  = "pd-ssd"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.bastion_subnet.self_link
    access_config {
      nat_ip = google_compute_address.bastion_ip.address
    }
  }

  service_account {
    email  = google_service_account.bastion_service_account.email
    scopes = ["userinfo-email", "storage-rw"]
  }
}