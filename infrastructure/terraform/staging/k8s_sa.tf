module "external_dns_service_account" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  version = "12.1.0"

  name                            = "external-dns-${var.env}"
  namespace                       = "tools"
  project_id                      = var.project_id
  automount_service_account_token = true
  roles                           = ["roles/dns.admin"]
}

module "cert_manager_service_account" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  version = "12.1.0"

  name                            = "cert-manager-${var.env}"
  namespace                       = "tools"
  project_id                      = var.project_id
  automount_service_account_token = true
  roles                           = ["roles/dns.admin"]
}

module "gitlab_service_account" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  version = "12.1.0"

  name                            = "gitlab-${var.env}"
  namespace                       = "gitlab"
  project_id                      = var.project_id
  automount_service_account_token = true
  roles                           = ["roles/container.admin", "roles/iam.serviceAccountUser"]
}

module "prometheus_service_account" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  version = "12.1.0"

  name                            = "k8s-prometheus"
  namespace                       = "prometheus"
  project_id                      = var.project_id
  automount_service_account_token = true
  roles                           = ["roles/compute.viewer"]
}

resource "google_service_account" "k8s_vault_kms_service_account" {
  account_id   = "k8s-vault"
  display_name = "Vault KMS for auto-unseal"
}

resource "google_storage_bucket_iam_member" "k8s_vault_service_account_iam_binding" {
  bucket = google_storage_bucket.k8s_vault.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.k8s_vault_kms_service_account.email}"
}

### Microservices SA ###

resource "google_service_account" "data_uploader_gcs_service_account" {
  account_id   = "data-uploader-${var.env}"
  display_name = "SA for GCS access for data-uploader microservice"
}

#resource "google_storage_bucket_acl" "data_uploader_service_account_acl" {
#  bucket      = google_storage_bucket.scan_data.name
#  role_entity = ["OWNER:user-${google_service_account.data_uploader_gcs_service_account.email}"]
#}

resource "google_storage_bucket_iam_member" "google_storage_data_uploader_iam_binding" {
  bucket = google_storage_bucket.scan_data.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.data_uploader_gcs_service_account.email}"
}

resource "google_service_account" "mri_service_gcs_service_account" {
  account_id   = "mri-service-${var.env}"
  display_name = "SA for GCS access for mri-service microservice"
}

#resource "google_storage_bucket_acl" "mri_service_service_account_acl" {
#  bucket      = google_storage_bucket.protocol_registry.name
#  role_entity = ["OWNER:user-${google_service_account.mri_service_gcs_service_account.email}"]
#}

resource "google_storage_bucket_iam_member" "google_storage_mri_service_iam_binding" {
  bucket = google_storage_bucket.protocol_registry.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.mri_service_gcs_service_account.email}"
}

resource "google_service_account" "integration_tests_gcs_service_account" {
  account_id   = "integration-tests-${var.env}"
  display_name = "SA for GCS access for integration tests"
}

resource "google_storage_bucket_iam_member" "google_storage_integration_tests_iam_binding" {
  bucket = google_storage_bucket.scan_data.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.integration_tests_gcs_service_account.email}"
}

resource "google_service_account" "gitlab_protocol_registry_gcs_service_account" {
  account_id   = "gitlab-proto-reg-gcs-${var.env}"
  display_name = "SA for GCS access for Gilab Protocol Registry GCS CI"
}

resource "google_storage_bucket_iam_member" "google_storage_gitlab_protocol_registry_iam_binding" {
  bucket = google_storage_bucket.protocol_registry.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gitlab_protocol_registry_gcs_service_account.email}"
}