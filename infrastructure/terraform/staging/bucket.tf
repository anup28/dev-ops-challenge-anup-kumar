resource "google_storage_bucket" "bastion" {
  name          = "devops-bastion-${var.env}"
  location      = "US"
  force_destroy = false
}

resource "google_storage_bucket" "k8s_vault" {
  name          = "devops-k8s-vault-${var.env}"
  location      = "US"
  force_destroy = false
}

resource "google_storage_bucket" "mdb_backups" {
  name          = "devops-mdb-backups-${var.env}"
  location      = "US"
  force_destroy = false
}

resource "google_storage_bucket" "scan_data" {
  name          = "scan-data-${var.env}"
  location      = "US"
  force_destroy = false
}

resource "google_storage_bucket" "protocol_registry" {
  name          = "protocol-registry-${var.env}"
  location      = "US"
  force_destroy = false
}