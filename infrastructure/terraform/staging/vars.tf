variable "project_id" {}
variable "region" {}
variable "env" {}

variable "customer_mdb_vpc_cidr" {}
variable "customer_mdb_node_count" {}
variable "customer_mdb_disk_size" {}
variable "customer_mdb_instance_type" {}

variable "bastion_vpc_cidr" {}
variable "bastion_user_list" {
  type = list(string)
}

variable "devops_gke_vpc_cidr" {}
variable "elasticsearch_vpc_cidr" {}

variable "iam_owner_users" {}
variable "iam_editor_users" {}
variable "iam_viewer_users" {}
variable "iam_sops_users" {}
variable "iam_ssh_keys" {}