terraform {
  backend "gcs" {
    bucket = "tf-devops-staging"
    prefix = "terraform/staging"
  }
}

provider "google" {
  project = var.project_id
  region  = "us-central1"
  zone    = "us-central1-c"
}

provider "local" {

}