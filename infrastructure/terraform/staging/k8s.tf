resource "google_compute_subnetwork" "devops_gke_subnet" {
  name          = "devops-gke-${var.env}-subnet"
  ip_cidr_range = var.devops_gke_vpc_cidr
  region        = var.region
  network       = module.vpc.network_self_link

  secondary_ip_range {
    range_name    = "devops-gke-pod-range"
    ip_cidr_range = "173.16.0.0/13" # 173.16.0.1 - 173.23.255.254
  }

  secondary_ip_range {
    range_name    = "devops-gke-service-range"
    ip_cidr_range = "173.24.0.0/20" # 173.24.0.1 - 173.24.15.254
  }
}

resource "google_service_account" "devops_gke_service_account" {
  account_id = "k8s-root"
}

resource "google_service_account" "devops_gke_microservices_service_account" {
  account_id = "k8s-microservices"
}

resource "google_service_account" "devops_gke_tools_service_account" {
  account_id = "k8s-tools"
}

resource "google_service_account" "devops_gke_elasticsearch_service_account" {
  account_id = "k8s-elasticsearch"
}

resource "google_compute_firewall" "k8s_master_pods_vault_8443" {
  name    = "k8s-master-to-pods-8443"
  network = module.vpc.network_self_link

  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }

  source_ranges = list("10.0.0.0/28")
}

module "devops_gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  project_id                 = var.project_id
  name                       = "devops-gke-1"
  region                     = "us-central1"
  zones                      = ["us-central1-a"] #, "us-central1-b", "us-central1-f"]
  network                    = module.vpc.network_name
  subnetwork                 = google_compute_subnetwork.devops_gke_subnet.name
  ip_range_pods              = "devops-gke-pod-range"
  ip_range_services          = "devops-gke-service-range"
  http_load_balancing        = false
  horizontal_pod_autoscaling = true
  network_policy             = true
  enable_private_endpoint    = true
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "10.0.0.0/28"
  create_service_account     = false
  service_account            = google_service_account.devops_gke_service_account.email
  master_authorized_networks = [
    {
      cidr_block   = "10.50.0.0/16"
      display_name = "internal-subnet"
    },
  ]
  master_global_access_enabled = false

  kubernetes_version = "1.17.12-gke.2502"
  logging_service    = "none"
  monitoring_service = "none"

  node_pools = [
    {
      name               = "microservices-node-pool"
      machine_type       = "n1-standard-1"
      min_count          = 0
      max_count          = 1
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = google_service_account.devops_gke_microservices_service_account.email
      preemptible        = true
      initial_node_count = 1
    },
    {
      name               = "tools-node-pool"
      machine_type       = "n1-standard-1"
      min_count          = 0
      max_count          = 2
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = google_service_account.devops_gke_tools_service_account.email
      preemptible        = true
      initial_node_count = 1
    },
    {
      name               = "prometheus-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 0
      max_count          = 1
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = module.prometheus_service_account.gcp_service_account_email
      preemptible        = true
      initial_node_count = 1
    },
    {
      name               = "elasticsearch-node-pool"
      machine_type       = "n1-standard-1"
      min_count          = 3
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = google_service_account.devops_gke_elasticsearch_service_account.email
      preemptible        = true
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {
      Name        = "devops-k8s-${var.env}"
      Project     = var.project_id
      Environment = var.env
      Terraform   = true
    }

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}
  }

  node_pools_tags = {
    all = []
  }
}
